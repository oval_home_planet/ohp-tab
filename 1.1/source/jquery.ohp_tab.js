(function($) {
    $.fn.ohp_tab = function(options){
        var defaults = {
            tabMenu  : '.tab-menu',
            tabContents  : '.tab-contents'
        };
        var setting = $.extend(defaults, options);

//-------------------------------

        var selfTabs = $(this).children(setting.tabMenu);
        var selfContents = $(this).children(setting.tabContents);

        $(this).addClass('ohp-tab');

		selfTabs.find('[role="tab"]').click(function(e){
			e.preventDefault();

			var id = $(this).attr('aria-controls');

			selfContents.children('[role="tabpanel"]').attr('aria-hidden',true);
			selfContents.children('#'+id).attr('aria-hidden',false);

			selfTabs.find('[role="tab"]').attr('aria-selected',false);
			$(this).attr('aria-selected',true);
		});

//-------------------------------

        return(this);
    };
})(jQuery);