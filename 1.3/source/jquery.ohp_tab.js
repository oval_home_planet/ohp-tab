(function($) {
    $.fn.ohp_tab = function(options){
        var defaults = {
            tabMenu     : '.tab-menu',
            tabContents : '.tab-contents',
            startTab    : 1
        };
        var setting = $.extend(defaults, options);

//-------------------------------

        var selfTabs = $(this).children(setting.tabMenu);
        var selfContents = $(this).children(setting.tabContents);

        selfTabs.addClass('tab');
        selfContents.addClass('tabpanel');

        $(this).addClass('ohp-tab');

        selfTabs.children('li').children('a').attr('role', 'tab').attr('aria-selected',false);
        selfTabs.children('li:nth-child('+setting.startTab+')').children('a').attr('aria-selected',true);
        selfTabs.children('li').children('a').each(function( i ){
			$(this).attr('aria-controls',$(this).attr('href').replace('#',''));
            selfContents.children($(this).attr('href')).attr('role','tabpanel').attr('aria-hidden',true).attr('aria-labelledby',$(this).attr('href').replace('#',''));
		});

        selfContents.children(selfTabs.children('li:nth-child('+setting.startTab+')').children('a').attr('href')).attr('aria-hidden',false);

		selfTabs.find('[role="tab"]').click(function(e){
			e.preventDefault();

			var id = $(this).attr('aria-controls');

			selfContents.children('[role="tabpanel"]').attr('aria-hidden',true);
			selfContents.children('#'+id).attr('aria-hidden',false);

			selfTabs.find('[role="tab"]').attr('aria-selected',false);
			$(this).attr('aria-selected',true);
		});

//-------------------------------

        return(this);
    };
})(jQuery);