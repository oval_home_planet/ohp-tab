(function($) {
    $.fn.ohp_tab = function(options){
        var defaults = {
        };
        var setting = $.extend(defaults, options);

//-------------------------------

		var self = $(this);

		self.find('[role="tab"]').click(function(e){
			e.preventDefault();

			var id = $(this).attr('aria-controls');

			self.find('[role="tabpanel"]').attr('aria-hidden',true);
			self.find('#'+id).attr('aria-hidden',false);

			self.find('[role="tab"]').attr('aria-selected',false);
			$(this).attr('aria-selected',true);
		});

//-------------------------------

        return(this);
    };
})(jQuery);